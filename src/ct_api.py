from urllib.request import urlopen
from codecs import open

query_url_exact_studies = "https://clinicaltrials.gov/api/query/full_studies?expr=AREA%5BNCTId%5DNCT01140906+OR+" \
                          "NCT00635219+OR+NCT00811252+OR+NCT00672620+OR+NCT01153009+OR+NCT01597908+OR+NCT01584648+OR+" \
                          "NCT01072175+OR+NCT01689519&min_rnk=1&max_rnk=10&fmt=xml"

# Depression
# NCT01140906, NCT00635219, NCT00811252, NCT00672620, NCT01153009

# Melanoma
# NCT01597908, NCT01584648, NCT01072175, NCT01689519


def retrieve_clinical_trials(full_study):
    """
    This function retrieves the clinical trials data and stores them
    :param full_study: is a boolean which indicates whether we want full studies or not
    :return: data gets stored in data.xml
    """
    if not isinstance(full_study, bool):
        return

    # query = query_url_full_studies if full_study else query_url_first_tests
    query = query_url_exact_studies

    output_file = open("../resources/data.xml", "w", "utf-8")
    data = urlopen(query).read().decode("utf8")

    output_file.write(data)
    output_file.close()
