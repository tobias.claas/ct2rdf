import time
import csv

from rdflib import Graph, Namespace, URIRef, Literal
from rdflib.plugins.sparql import prepareQuery
from rdflib.plugins.sparql.processor import SPARQLResult

CTRO = Namespace("http://www.semanticweb.org/root/ontologies/2018/6/ctro#")

header_study = ["NCTId", "Publication: Year and Author", "CTDesign", "Precondition", "Gender",
                "Arm1 (drug,dose,unit,frequency,delivery method)", "Arm2 (drug,dose,unit,frequency,delivery method)",
                "N.Participants Arm1 started", "N.Participants Arm2 started",
                "N.Participants Arm1 completed", "N.Participants Arm2 completed",
                "N.Participants Arm1 left", "N.Participants Arm2 left",
                "avg. Age Arm1", "avg. Age Arm2"]  # length 5+2+2+2+2+2=15
header_outcome = ["Primary Outcome (Id, EndpointDescription, aggregation method)",
                  "Baseline (units)", "ChangeValue (SD) Arm1, changeDirection", "ChangeValue (SD) Arm2, changeDirection",
                  "Adverse effect1 (aggregation unit)", "numberAffected Arm1", "numberAffected Arm2",
                  "Adverse effect2 (aggregation unit)", "numberAffected Arm1", "numberAffected Arm2",
                  "\t", "\t", "\t", "\t", "\t"]  # length 1+3+3+3(+3) = 13

arm_label_list = []
results_study = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
results_outcome = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
description = []


def append_to_result_list(res_list, value, index):
    if isinstance(value, SPARQLResult):
        for row in value:
            for elem in row:
                res_list[index].append(elem)
    else:
        res_list[index].append(value)


def generate_result_tsv_line(result_list, i):
    tsv_line = []
    for values in result_list:
        if len(values) > i:
            tsv_line.append(values[i])
        else:
            tsv_line.append("")
    return tsv_line


def print_in_tsv_file(study_id):
    with open('../resources/evaluation/evaluation_table_' + study_id + '.tsv', 'w', newline='') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerow(header_study)
        max_depth = max([len(values) for values in results_study])
        for i in range(0, max_depth):
            result_line = generate_result_tsv_line(results_study, i)
            tsv_writer.writerow(result_line)

        tsv_writer.writerow(["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""])  # print empty row

        tsv_writer.writerow(header_outcome)
        max_depth = max([len(values) for values in results_outcome])
        for i in range(0, max_depth):
            result_line = generate_result_tsv_line(results_outcome, i)
            tsv_writer.writerow(result_line)


def generate_evaluation_table_study(study_id, knowledge_base):
    i = 0
    append_to_result_list(results_study, study_id, i)      # NCTId, provided
    i += 1
    study_id = URIRef(CTRO + study_id)

    # Publication: Year and Author
    query_publication = prepareQuery("""SELECT DISTINCT ?author ?date
                              WHERE {
                                ?pub a ctro:Publication ;
                                  ctro:hasAuthor ?author ;
                                  ctro:hasPublicationYear ?date ;
                                  ctro:describes ?ct .
                                ?ct a ctro:ClinicalTrial}""", initNs={"ctro": CTRO})
    result_publication = knowledge_base.query(query_publication, initBindings={'ct': study_id})
    append_to_result_list(results_study, result_publication, i)
    i += 1

    # ct design
    query_ct_design = prepareQuery("""SELECT DISTINCT ?ct_design
                              WHERE {
                                ?ct a ctro:ClinicalTrial ;
                                  ctro:hasCTDesign ?ct_design . }""", initNs={"ctro": CTRO})
    result_ct_design = knowledge_base.query(query_ct_design, initBindings={'ct': study_id})
    append_to_result_list(results_study, result_ct_design, i)
    i += 1

    # preconditions
    query_precondition = prepareQuery("""SELECT DISTINCT ?pre_cond
                              WHERE {
                                ?ct a ctro:ClinicalTrial ;
                                  ctro:hasPopulation ?pop . 
                                ?pop ctro:hasPrecondition ?pre_cond .}""", initNs={"ctro": CTRO})
    result_precondition = knowledge_base.query(query_precondition, initBindings={'ct': study_id})
    append_to_result_list(results_study, result_precondition, i)
    i += 1

    query_gender = prepareQuery("""SELECT DISTINCT ?gender
                              WHERE {
                                ?ct a ctro:ClinicalTrial .
                                ?ct ctro:hasPopulation ?pop . 
                                ?pop ctro:hasGender ?gender .}""", initNs={"ctro": CTRO})
    result_gender = knowledge_base.query(query_gender, initBindings={'ct': study_id})
    append_to_result_list(results_study, result_gender, i)
    i += 1  # 5

    # arm/group label
    query_arm = prepareQuery("""SELECT DISTINCT ?label
                              WHERE {
                                ?ct a ctro:ClinicalTrial .
                                ?ct ctro:hasArm ?arm . 
                                ?arm ctro:hasLabel ?label .}""", initNs={"ctro": CTRO})
    result_arm = knowledge_base.query(query_arm, initBindings={'ct': study_id})
    for row in result_arm:  # add to arms/groups but store label to identify them
        for elem in row:
            arm_label_list.append(elem)
    arm_label_list.sort()

    for j in range(0, len(arm_label_list)):
        if j >= 2:
            continue

        label = arm_label_list[j]
        append_to_result_list(results_study, "label: " + label, i + j)

        query_has_drug = prepareQuery("""SELECT DISTINCT ?drug_bool
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasIntervention ?int .
                                    ?med ctro:hasDrug ?drug_bool .}""", initNs={"ctro": CTRO})
        result_has_drug = knowledge_base.query(query_has_drug, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_has_drug, i + j)

        query_has_drug = prepareQuery("""SELECT DISTINCT ?dose
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasIntervention ?int .
                                    ?int ctro:hasMedication ?med .
                                    ?med ctro:hasDoseValue ?dose .}""", initNs={"ctro": CTRO})
        result_has_drug = knowledge_base.query(query_has_drug, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_has_drug, i + j)

        # unit of the drug
        query_has_drug = prepareQuery("""SELECT DISTINCT ?unit
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasIntervention ?int .
                                    ?int ctro:hasMedication ?med .
                                    ?med ctro:hasDoseUnit ?unit .}""", initNs={"ctro": CTRO})
        result_has_drug = knowledge_base.query(query_has_drug, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_has_drug, i + j)

        query_has_drug = prepareQuery("""SELECT DISTINCT ?freq
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasIntervention ?int .
                                    ?int ctro:hasFrequency ?freq .}""", initNs={"ctro": CTRO})
        result_has_drug = knowledge_base.query(query_has_drug, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_has_drug, i + j)

        query_has_drug = prepareQuery("""SELECT DISTINCT ?method
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasIntervention ?int .
                                    ?int ctro:hasMedication ?med .
                                    ?med ctro:hasDeliveryMethod ?method .}""", initNs={"ctro": CTRO})
        result_has_drug = knowledge_base.query(query_has_drug, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_has_drug, i + j)

        # arm/group num participants started
        query_arm_num_part = prepareQuery("""SELECT DISTINCT ?num
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasNumberOfPatientsArm ?num .
                                    ?arm ctro:hasLabel ?label .}""", initNs={"ctro": CTRO})
        result_arm_num_part = knowledge_base.query(query_arm_num_part, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_arm_num_part, i + j + 2)

        # arm/group num participants completed
        query_arm_num_part = prepareQuery("""SELECT DISTINCT ?num
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasFinalNumPatientsArm ?num .
                                    ?arm ctro:hasLabel ?label .}""", initNs={"ctro": CTRO})
        result_arm_num_part = knowledge_base.query(query_arm_num_part, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_arm_num_part, i + j + 4)

        # arm/group num participants left
        query_arm_num_part = prepareQuery("""SELECT DISTINCT ?num
                                  WHERE {
                                    ?ct ctro:hasArm ?arm . 
                                    ?arm a ctro:Arm .
                                    ?arm ctro:hasNumPatientsLeftArm ?num .
                                    ?arm ctro:hasLabel ?label .}""", initNs={"ctro": CTRO})
        result_arm_num_part = knowledge_base.query(query_arm_num_part, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_arm_num_part, i + j + 6)

        # avg age arm/group
        query_avg_age = prepareQuery("""SELECT DISTINCT ?avg_age
                                          WHERE {
                                            ?ct ctro:hasArm ?arm . 
                                            ?arm a ctro:Arm .
                                            ?arm ctro:hasAvgAge ?avg_age .
                                            ?arm ctro:hasLabel ?label .}""", initNs={"ctro": CTRO})
        result_avg_age = knowledge_base.query(query_avg_age, initBindings={'ct': study_id, 'label': label})
        append_to_result_list(results_study, result_avg_age, i + j + 8)


def generate_evaluation_table_outcome(study_id, knowledge_base):
    global arm_label_list

    study_id = URIRef(CTRO + study_id)
    label = arm_label_list[0]
    outcome_id_list = []  # one id for first primary outcome of every arm, in this case two
    i = 0

    # get primary outcome of arm 1
    query = prepareQuery("""SELECT DISTINCT ?out
                        WHERE {
                            ?ct a ctro:ClinicalTrial .
                            ?ct ctro:hasArm ?arm .
                            ?arm a ctro:Arm .
                            ?arm ctro:hasLabel ?label .
                            ?arm ctro:hasOutcome ?out .
                            ?out a ctro:Outcome .
                            ?out ctro:hasOutcomeType ?type .} 
                            ORDER BY DESC(?out) 
                            LIMIT 1""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'ct': study_id, 'label': label,
                                                       'type': Literal("Primary")})
    # Note: order by ?out in query
    outcome_id_1 = [y for x in result for y in x][0]
    outcome_id_list.append(outcome_id_1)
    append_to_result_list(results_outcome, outcome_id_1, i)

    # get primary outcome of arm 2
    query = prepareQuery("""SELECT DISTINCT ?out
                        WHERE {
                            ?ct a ctro:ClinicalTrial .
                            ?ct ctro:hasArm ?arm .
                            ?arm a ctro:Arm .
                            ?arm ctro:hasLabel ?label .
                            ?arm ctro:hasOutcome ?out .
                            ?out a ctro:Outcome .
                            ?out ctro:hasOutcomeType ?type .} 
                            ORDER BY DESC(?out) 
                            LIMIT 1""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'ct': study_id, 'label': arm_label_list[1],
                                                       'type': Literal("Primary")})
    # Note: order by ?out in query
    outcome_id_2 = [y for x in result for y in x][0]
    outcome_id_list.append(outcome_id_2)
    append_to_result_list(results_outcome, outcome_id_2, i)

    # The following attributes should be the same for both outcomes since since only the drugs and measures differ
    # Extract endpoint-description
    query = prepareQuery("""SELECT DISTINCT ?desc
                        WHERE {
                            ?out a ctro:Outcome .
                            ?out ctro:hasEndpoint ?end .
                            ?end ctro:hasEndpointDescription ?desc}""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'out': outcome_id_1})
    append_to_result_list(results_outcome, result, i)

    # Extract aggregation-method
    query = prepareQuery("""SELECT DISTINCT ?method
                            WHERE {
                            ?out a ctro:Outcome .
                            ?out ctro:hasEndpoint ?end .
                            ?end ctro:hasAggregationMethod ?method}""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'ct': study_id, 'out': outcome_id_1})
    append_to_result_list(results_outcome, result, i)
    i += 1

    # Extract Baseline-Unit
    query = prepareQuery("""SELECT DISTINCT ?unit
                            WHERE {
                            ?out a ctro:Outcome .
                            ?out ctro:hasEndpoint ?end .
                            ?end a ctro:Endpoint .
                            ?end ctro:hasBaselineUnit ?unit .}""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'out': outcome_id_1})
    append_to_result_list(results_outcome, result, i)
    i += 1

    for j in range(0, len(outcome_id_list)):  # outcome_id_list has length 2
        # extract ChangeValue (SD) Arm j
        outcome_id = outcome_id_list[j]
        query = prepareQuery("""SELECT DISTINCT ?val  
                                WHERE {
                                ?out ctro:hasChangeValue ?val ;
                                    ctro:hasOutcomeType ?type}""", initNs={'ctro': CTRO})
        result = knowledge_base.query(query, initBindings={'out': outcome_id})
        append_to_result_list(results_outcome, result, i)

        # hasChangeDir
        query = prepareQuery("""SELECT DISTINCT ?dir
                                WHERE {
                                ?out a ctro:Outcome .
                                ?out ctro:hasOutcomeType ?type .
                                ?out ctro:hasChangeDirection ?dir .}""", initNs={'ctro': CTRO})
        result = knowledge_base.query(query, initBindings={'out': outcome_id})
        append_to_result_list(results_outcome, result, i)
        i += 1

    # adverse effects
    query = prepareQuery("""SELECT DISTINCT ?desc
                            WHERE {
                            ?ct a ctro:ClinicalTrial .
                            ?ct ctro:hasArm ?arm .
                            ?arm ctro:hasLabel ?label .
                            ?arm ctro:hasAdverseEffect ?ae .
                            ?ae ctro:hasEndpoint ?end .
                            ?end ctro:hasEndpointDescription ?desc}""", initNs={'ctro': CTRO})
    result = knowledge_base.query(query, initBindings={'ct': study_id, 'label': label})
    # select interesting adverse effects, namely hypertension and diarrhea
    adverse_effect_description_list = []
    for row in result:  # get adverse effects hypertension and diarrhea due to a lot ofa occurrences
        for desc in row:
            if desc == Literal("Hypertension") or desc == Literal("Diarrhea") or desc == Literal("Diarrhoea"):
                adverse_effect_description_list.append(desc)

    if len(adverse_effect_description_list) < 2:  # if less than two adverse effects are in list add some
        adverse_effect_description_list.sort()  # make sure they are the same along the studies with sort
        for row in result:
            for desc in row:
                adverse_effect_description_list.append(desc)
                if len(adverse_effect_description_list) >= 2:
                    break
            if len(adverse_effect_description_list) >= 2:
                break

    for j in range(0, len(adverse_effect_description_list)):  # length must be exactly two
        pivot_description = adverse_effect_description_list[j]
        append_to_result_list(results_outcome, pivot_description, i + j * 3)
        i += 1

        # extract for this ae the number of affected people in arm 1
        label = arm_label_list[0]
        query = prepareQuery("""SELECT DISTINCT ?num
                                    WHERE {
                                    ?ct a ctro:ClinicalTrial .
                                    ?ct ctro:hasArm ?arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasAdverseEffect ?ae .
                                    ?ae ctro:hasEndpoint ?end .
                                    ?ae ctro:hasNumberAffected ?num .
                                    ?end ctro:hasEndpointDescription ?desc}
                                    ORDER BY ASC(?ae)
                                    LIMIT 2""", initNs={'ctro': CTRO})
        result = knowledge_base.query(query, initBindings={'ct': study_id, 'label': label, 'desc': pivot_description})
        append_to_result_list(results_outcome, result, i + j * 3)
        i += 1

        # extract for this ae the number of affected people in arm 2
        label = arm_label_list[1]
        query = prepareQuery("""SELECT DISTINCT ?num
                                    WHERE {
                                    ?ct a ctro:ClinicalTrial .
                                    ?ct ctro:hasArm ?arm .
                                    ?arm ctro:hasLabel ?label .
                                    ?arm ctro:hasAdverseEffect ?ae .
                                    ?ae ctro:hasEndpoint ?end .
                                    ?ae ctro:hasNumberAffected ?num .
                                    ?end ctro:hasEndpointDescription ?desc}
                                    ORDER BY ASC(?ae)
                                    LIMIT 2""", initNs={'ctro': CTRO})
        result = knowledge_base.query(query, initBindings={'ct': study_id, 'label': label, 'desc': pivot_description})
        append_to_result_list(results_outcome, result, i + j * 3)
        i += 1
        i -= 3


def setup(study_id_list, kb):
    global arm_label_list
    global results_study
    global results_outcome
    global description

    if kb is None:
        knowledge_base = Graph()
        knowledge_base.load("../resources/knowledge_base.ttl", format="turtle")
    else:
        knowledge_base = kb

    for study_id in study_id_list:
        arm_label_list = []
        results_study = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
        results_outcome = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
        description = []

        generate_evaluation_table_study(study_id, knowledge_base)
        generate_evaluation_table_outcome(study_id, knowledge_base)

        print_in_tsv_file(study_id)


if __name__ == '__main__':
    start = time.time()
    study_list = ["NCT01140906", "NCT00635219", "NCT00811252", "NCT00672620", "NCT01153009", "NCT01597908",
                  "NCT01584648", "NCT01072175", "NCT01689519"]
    setup(study_list, None)
    end = time.time()
    print(end - start)
