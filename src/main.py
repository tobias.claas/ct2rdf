from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF
from codecs import open
import time
import re
import rdfextras
import xml.etree.ElementTree as et
import ct_api


###########################################################
# Defining namespaces
###########################################################
CTRO = Namespace("http://www.semanticweb.org/root/ontologies/2018/6/ctro#")
# data types
CTRO_ClinicalTrial = URIRef(CTRO + "ClinicalTrial")
CTRO_Publication = URIRef(CTRO + "Publication")
CTRO_Population = URIRef(CTRO + "Population")
CTRO_Arm = URIRef(CTRO + "Arm")
CTRO_Intervention = URIRef(CTRO + "Intervention")
CTRO_Medication = URIRef(CTRO + "Medication")
CTRO_Outcome = URIRef(CTRO + "Outcome")
CTRO_Endpoint = URIRef(CTRO + "Endpoint")
CTRO_EvidenceQuality = URIRef(CTRO + "EvidenceQuality")
CTRO_DiffBetweenGroups = URIRef(CTRO + "DiffBetweenGroups")
# predicates for describing relations between entities
CTRO_describes = URIRef(CTRO + "describes")
CTRO_hasPopulation = URIRef(CTRO + "hasPopulation")
CTRO_hasArm = URIRef(CTRO + "hasArm")
CTRO_hasIntervention = URIRef(CTRO + "hasIntervention")
CTRO_hasMedication = URIRef(CTRO + "hasMedication")
CTRO_hasOutcome = URIRef(CTRO + "hasOutcome")
CTRO_hasEndpoint = URIRef(CTRO + "hasEndpoint")
CTRO_hasEvidenceQualityIndicator = URIRef(CTRO + "hasEvidenceQualityIndicator")
CTRO_hasDiffBetweenGroups = URIRef(CTRO + "hasDiffBetweenGroups")
CTRO_hasAdverseEffect = URIRef(CTRO + "hasAdverseEffect")
CTRO_hasOutcome1 = URIRef(CTRO + "hasOutcome1")
CTRO_hasOutcome2 = URIRef(CTRO + "hasOutcome2")
# predicates for attributes in entity
CTRO_hasNCTId = URIRef(CTRO + "hasNTCId")
CTRO_hasObjectiveDescription = URIRef(CTRO + "hasObjectiveDescription")
CTRO_hasBriefSummary = URIRef(CTRO + "hasShortSummary")
CTRO_hasNumberPatientsCT = URIRef(CTRO + "hasNumberPatientsCT")
CTRO_hasCTDuration = URIRef(CTRO + "hasCTDuration")
CTRO_hasAllocationRatio = URIRef(CTRO + "hasAllocationRatio")
CTRO_hasCTAnalysisApproach = URIRef(CTRO + "hasCTAnalysisApproach")
CTRO_hasCTDesign = URIRef(CTRO + "hasCTDesign")
CTRO_hasConclusionComment = URIRef(CTRO + "hasConclusionComment")
CTRO_analysesHealthCondition = URIRef(CTRO + "analysesHealthCondition")
CTRO_hasFinalNumPatientsCT = URIRef(CTRO + "hasFinalNumPatientsCT")
CTRO_hasNumPatientsLeftCT = URIRef(CTRO + "hasNumPatientsLeftCT")
CTRO_hasRelNumPatientsLeftCT = URIRef(CTRO + "hasRelNumPatientsLeftCT")
# predicates for attributes in publication
CTRO_hasTitle = URIRef(CTRO + "hasTitle")
CTRO_hasAuthor = URIRef(CTRO + "hasAuthor")
CTRO_hasPublicationYear = URIRef(CTRO + "hasPublicationYear")
CTRO_hasPMID = URIRef(CTRO + "hasPMID")
# predicates for attributes in population
CTRO_hasMinAge = URIRef(CTRO + "hasMinAge")
CTRO_hasMaxAge = URIRef(CTRO + "hasMaxAge")
CTRO_hasAvgAge = URIRef(CTRO + "hasAvgAge")
CTRO_hasPrecondition = URIRef(CTRO + "hasPrecondition")
CTRO_hasGender = URIRef(CTRO + "hasGender")
CTRO_hasEthnicity = URIRef(CTRO + "hasEthnicity")
CTRO_hasCountry = URIRef(CTRO + "hasCountry")
# predicates for attributes in arm
CTRO_hasNumberOfPatientsArm = URIRef(CTRO + "hasNumberOfPatientsArm")
CTRO_hasFinalNumPatientsArm = URIRef(CTRO + "hasFinalNumPatientsArm")
CTRO_hasNumPatientsLeftArm = URIRef(CTRO + "hasNumPatientsLeftArm")
CTRO_hasRelNumPatientsLeftArm = URIRef(CTRO + "hasRelNumPatientsLeftArm")
CTRO_hasLabel = URIRef(CTRO + "hasLabel")
# predicates for attributes in Intervention
CTRO_hasFrequency = URIRef(CTRO + "hasFrequency")
CTRO_hasInterval = URIRef(CTRO + "hasInterval")
CTRO_hasDuration = URIRef(CTRO + "hasDuration")
CTRO_hasRelativeFreqTime = URIRef(CTRO + "hasRelativeFreqTime")
# predicates for attributes in Medication
CTRO_hasDrug = URIRef(CTRO + "hasDrug")
CTRO_hasDoseValue = URIRef(CTRO + "hasDoseValue")
CTRO_hasDoseUnit = URIRef(CTRO + "hasDoseUnit")
CTRO_hasDeliveryMethod = URIRef(CTRO + "hasDeliveryMethod")
CTRO_hasApplicationCondition = URIRef(CTRO + "hasApplicationCondition")
# predicates for attributes in outcome
CTRO_hasOutcomeType = URIRef(CTRO + "hasOutcomeType")
CTRO_hasBaselineValue = URIRef(CTRO + "hasBaselineValue")
CTRO_hasPValueBL = URIRef(CTRO + "hasPValueBL")
CTRO_hasStandardDevBL = URIRef(CTRO + "hasStandardDevBL")
CTRO_hasStandardErrorBL = URIRef(CTRO + "hasStandardErrorBL")
CTRO_hasConfidenceIntervalBL = URIRef(CTRO + "hasConfidenceIntervalBL")
CTRO_hasResultMeasuredValue = URIRef(CTRO + "hasResultMeasuredValue")
CTRO_hasPValueResValue = URIRef(CTRO + "hasPValueResValue")
CTRO_hasStandardDevResValue = URIRef(CTRO + "hasStandardDevResValue")
CTRO_hasStandardErrorResValue = URIRef(CTRO + "hasStandardErrorResValue")
CTRO_hasConfidenceIntervalResValue = URIRef(CTRO + "hasConfidenceIntervalResValue")
CTRO_hasChangeValue = URIRef(CTRO + "hasChangeValue")
CTRO_hasPValueChangeValue = URIRef(CTRO + "hasPValueChangeValue")
CTRO_hasStandardDevChangeVal = URIRef(CTRO + "hasStandardDevChangeVal")
CTRO_hasStandardErrorChangeVal = URIRef(CTRO + "hasStandardErrorChangeVal")
CTRO_hasConfidenceIntervalChangeVal = URIRef(CTRO + "hasConfidenceIntervalChangeVal")
CTRO_hasRelativeChangeValue = URIRef(CTRO + "hasRelativeChangeValue")
CTRO_hasNumberAffected = URIRef(CTRO + "hasNumberAffected")
CTRO_hasPercentageAffected = URIRef(CTRO + "hasPercentageAffected")
CTRO_hasPValueNumAffected = URIRef(CTRO + "hasPValueNumAffected")
CTRO_hasStandardDevNumAffected = URIRef(CTRO + "hasStandardDevNumAffected")
CTRO_hasStandardErrorNumAffected = URIRef(CTRO + "hasStandardErrorNumAffected")
CTRO_hasConfidenceIntervalNumAffected = URIRef(CTRO + "hasConfidenceIntervalNumAffected")
CTRO_hasObservedResult = URIRef(CTRO + "hasObservedResult")
CTRO_hasTimePoint = URIRef(CTRO + "hasTimePoint")
CTRO_hasSubGroupDescription = URIRef(CTRO + "hasSubGroupDescription")
CTRO_hasChangeDirection = URIRef(CTRO + "hasChangeDirection")
# predicates for entity endpoint
CTRO_hasEndpointDescription = URIRef(CTRO + "hasEndpointDescription")
CTRO_hasAggregationMethod = URIRef(CTRO + "hasAggregationMethod")
CTRO_hasBaselineUnit = URIRef(CTRO + "hasBaselineUnit")
CTRO_hasMeasurementDevice = URIRef(CTRO + "hasMeasurementDevice")
# predicates for entity evidence_quality
CTRO_isPharmacySponsored = URIRef(CTRO + "isPharmacySponsored")
CTRO_hasConflictInterest = URIRef(CTRO + "hasConflictInterest")
CTRO_hasSponsorType = URIRef(CTRO + "hasSponsorType")
# predicates for entity DiffBetweenGroups
CTRO_hasDiffGroupRelativeValue = URIRef(CTRO + "hasDiffGroupRelativeValue")
CTRO_hasDiffGroupAbsValue = URIRef(CTRO + "hasDiffGroupAbsValue")
CTRO_hasPValueDiff = URIRef(CTRO + "hasPValueDiff")
CTRO_hasConfidenceIntervalDiff = URIRef(CTRO + "hasConfidenceIntervalDiff")
CTRO_hasStandardDevDiff = URIRef(CTRO + "hasStandardDevDiff")
CTRO_hasStandardErrorDiff = URIRef(CTRO + "hasStandardErrorDiff")
###########################################################
# Global Variables
###########################################################
knowledge_base = Graph()
knowledge_base.namespace_manager.bind("ctro", CTRO)
ct_gov_prefix = "https://clinicaltrials.gov/ct2/show/"

###########################################################
# Defining dictionaries
###########################################################
lookup_table_study = [
    {"name": "NCTId", "predicate": CTRO_hasNCTId, "conditions": {}},
    {"name": "DetailedDescription", "predicate": CTRO_hasObjectiveDescription, "conditions": {}},
    {"name": "EnrollmentCount", "predicate": CTRO_hasNumberPatientsCT, "conditions": {"EnrollmentType": "Actual"}},
    {"name": "TargetDuration", "predicate": CTRO_hasCTDuration, "conditions": {}},
    # hasAllocationRatio  # TODO, what is this
    {"name": "OutcomeAnalysisNonInferiorityType", "predicate": CTRO_hasCTAnalysisApproach, "conditions": {}},
    {"name": "DesignAllocation", "predicate": CTRO_hasCTDesign, "conditions": {}},
    {"name": "DesignMasking", "predicate": CTRO_hasCTDesign, "conditions": {}},
    {"name": "DesignWhoMasked", "predicate": CTRO_hasCTDesign, "conditions": {}},
    {"name": "BriefSummary", "predicate": CTRO_hasConclusionComment, "conditions": {}},
    {"name": "ConditionMeshTerm", "predicate": CTRO_analysesHealthCondition, "conditions": {}},
    {"name": "FlowAchievementNumSubjects", "predicate": CTRO_hasFinalNumPatientsCT,
     "conditions": {"FlowMilestoneType": "COMPLETED"}},
    {"name": "FlowAchievementNumSubjects", "predicate": CTRO_hasNumPatientsLeftCT,
     "conditions": {"FlowMilestoneType": "NOT COMPLETED"}}
    # hasRelNumPatientsLeftCT
    # extract from text of calculate
]

lookup_table_publication = [
    {"name": "OfficialTitle", "predicate": CTRO_hasTitle, "conditions": {}},
    {"name": "PointOfContactTitle", "predicate": CTRO_hasAuthor, "conditions": {}},
    {"name": "ResultsFirstPostDate", "predicate": CTRO_hasPublicationYear, "conditions": {}},
    # Journal not found
    {"name": "ReferencePMID", "predicate": CTRO_hasPMID, "conditions": {}},
]

lookup_table_population = [
    {"name": "MinimumAge", "predicate": CTRO_hasMinAge, "conditions": {}},
    {"name": "MaximumAge", "predicate": CTRO_hasMaxAge, "conditions": {}},
    {"name": "Condition", "predicate": CTRO_hasPrecondition, "conditions": {}},
    {"name": "Gender", "predicate": CTRO_hasGender, "conditions": {}},
    # BaselinePopulationDescription or EligbilityCriteria or BaselineMeasureTitle
    # and then BaselineCategoryTitle is ethnicity
    {"name": "LocationCountry", "predicate": CTRO_hasCountry, "conditions": {}}
]

lookup_table_arm = [
    {"name": "FlowAchievementNumSubjects", "predicate": CTRO_hasNumberOfPatientsArm,
     "conditions": {"FlowMilestoneType": "STARTED"}},
    {"name": "FlowAchievementNumSubjects", "predicate": CTRO_hasFinalNumPatientsArm,
     "conditions": {"FlowMilestoneType": "COMPLETED"}},
    {"name": "FlowAchievementNumSubjects", "predicate": CTRO_hasNumPatientsLeftArm,
     "conditions": {"FlowMilestoneType": "NOT COMPLETED"}},
    {"name": "ArmGroupLabel", "predicate": CTRO_hasLabel, "conditions": {}},
    {"name": "", "predicate": CTRO_hasAvgAge, "conditions": {}}
]

lookup_table_intervention = [
    {"name": "InterventionDescription", "predicate": CTRO_hasFrequency, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasInterval, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasDuration, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasRelativeFreqTime, "conditions": {}}
]

lookup_table_medication = [
    {"name": "InterventionDescription", "predicate": CTRO_hasDrug, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasDoseValue, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasDoseUnit, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasDeliveryMethod, "conditions": {}},
    {"name": "InterventionDescription", "predicate": CTRO_hasApplicationCondition, "conditions": {}},
]

lookup_table_outcome = [
    # baseline_measurement
    {"name": "OutcomeMeasureType", "predicate": CTRO_hasOutcomeType, "conditions": {}},
    {"name": "BaselineMeasurementValue", "predicate": CTRO_hasBaselineValue, "conditions": {}},
    {"name": "", "predicate": CTRO_hasPValueBL, "conditions": {}},
    {"name": "BaselineMeasurementValue", "predicate": CTRO_hasStandardDevBL, "conditions": {}},
    {"name": "BaselineMeasurementSpread", "predicate": CTRO_hasStandardErrorBL, "conditions": {}},
    {"name": "BaselineMeasureDispersionType", "predicate": CTRO_hasConfidenceIntervalBL,
     "conditions": {"BaselineMeasureDispersionType": "Standard Deviation"}},
    # outcome_analysis[j]
    {"name": "OutcomeMeasurementValue", "predicate": CTRO_hasResultMeasuredValue, "conditions": {}},
    {"name": "OutcomeAnalysisPValue", "predicate": CTRO_hasPValueResValue, "conditions": {}},
    {"name": "OutcomeAnalysisDispersionValue", "predicate": CTRO_hasStandardDevResValue,
     "conditions": {"OutcomeAnalysisDispersionType": "Standard Deviation"}},
    {"name": "OutcomeAnalysisDispersionValue", "predicate": CTRO_hasStandardErrorResValue,
     "conditions": {"OutcomeAnalysisDispersionType": "Standard Error"}},
    {"name": "OutcomeMeasureDispersionType", "predicate": CTRO_hasConfidenceIntervalResValue, "conditions": {}},
    # need to be extracted from text
    {"name": "", "predicate": CTRO_hasChangeValue, "conditions": {}},
    {"name": "", "predicate": CTRO_hasPValueChangeValue, "conditions": {}},
    {"name": "", "predicate": CTRO_hasStandardDevChangeVal, "conditions": {}},
    {"name": "", "predicate": CTRO_hasStandardErrorChangeVal, "conditions": {}},
    {"name": "", "predicate": CTRO_hasConfidenceIntervalChangeVal, "conditions": {}},
    {"name": "", "predicate": CTRO_hasRelativeChangeValue, "conditions": {}},
    # for adverse events only
    {"name": "SeriousEventStatsNumAffected", "predicate": CTRO_hasNumberAffected, "conditions": {}},
    {"name": "", "predicate": CTRO_hasPercentageAffected, "conditions": {}},
    {"name": "", "predicate": CTRO_hasPValueNumAffected, "conditions": {}},
    {"name": "", "predicate": CTRO_hasStandardDevNumAffected, "conditions": {}},
    {"name": "", "predicate": CTRO_hasStandardErrorNumAffected, "conditions": {}},
    {"name": "", "predicate": CTRO_hasConfidenceIntervalNumAffected, "conditions": {}},
    {"name": "OutcomeMeasureDescription", "predicate": CTRO_hasObservedResult, "conditions": {}},
    {"name": "", "predicate": CTRO_hasTimePoint, "conditions": {}},
    {"name": "", "predicate": CTRO_hasSubGroupDescription, "conditions": {}},
    {"name": "", "predicate": CTRO_hasChangeDirection, "conditions": {}},
]

lookup_table_endpoint = [
    {"name": "OutcomeMeasureDescription", "predicate": CTRO_hasEndpointDescription, "conditions": {}},
    {"name": "OutcomeAnalysisParamType", "predicate": CTRO_hasAggregationMethod, "conditions": {}},
    {"name": "OutcomeMeasureUnitOfMeasure", "predicate": CTRO_hasBaselineUnit, "conditions": {}},
    {"name": "", "predicate": CTRO_hasMeasurementDevice, "conditions": {}},
]

alternative_attribute_dict = {
    "DetailedDescription": "BriefSummary",
    "InterventionDescription": "InterventionName"

}


###########################################################
# Code section
###########################################################
def attribute_exists(xml_tree, attribute_name):
    """
    Checks whether an attribute exists for a particular clinical trial.
    :param xml_tree: particular clinical trial
    :param attribute_name: name of the attribute
    :return: true if exists, false if None and thus not exist
    """
    return True if xml_tree.find('.//Field[@Name="' + attribute_name + '"]') is not None else False


def check_conditions_easy(xml_parent, conditions):
    """
    Checks the conditions but xml-parent ist given.
    :param xml_parent parent or above of to-check attribute
    :param conditions that must be fulfilled
    :return: True if to-check attribute exists and is value, False otherwise
    """
    for condition_name in conditions:
        attribute = xml_parent.find('.//Field[@Name="' + condition_name + '"]')
        if attribute is None:
            return False
        actual_value = attribute.text
        if actual_value is not None and actual_value == conditions[condition_name]:
            return True

        return False


def check_conditions(attribute, conditions, parent_map):
    """
    For some attributes conditions must be fulfilled. The method checks them. The assumption is that the
    attribute itself is given and the attribute we have conditions to is on the same level.
    :param attribute: attribute that should fulfill conditions
    :param conditions: Dict of conditions
    :param parent_map:
    :return:
    """
    parent = parent_map[attribute]  # lookup parent

    for condition_name in conditions:  # check every condition
        condition = parent.find('.//Field[@Name="' + condition_name + '"]')
        if condition is None or condition.text != conditions[condition_name]:
            return False
    return True


def sum_attributes(xml_tree, index, lookup_table):
    # get all attributes with find_attributes -> Conditions fulfilled
    # sum them together
    # add with add_...
    """
    Sums all participants in FlowMilestoneList list
    :param xml_tree:
    :param index:
    :param lookup_table:
    :return:
    """
    flow_milestones = xml_tree.findall('.//Struct[@Name="FlowMilestone"]')
    condition = lookup_table[index].get("conditions")

    index_flow_milestones = []
    for i in range(0, len(flow_milestones)):
        if check_conditions_easy(flow_milestones[i], condition):  # needed to have correct xml_struct
            index_flow_milestones.append(i)

    if len(index_flow_milestones) == 0:
        return None

    attributes = []
    for i in index_flow_milestones:
        [attributes.append(value) for value in find_attributes(flow_milestones[i], index, lookup_table)]

    num = 0
    for attribute in attributes:
        num += int(attribute)

    return num


def find_attributes(xml_tree, index, lookup_dict):
    """
    Find according attribute. Check the condition.
    :param index:
    :param xml_tree: place to search at in xml tree
    :param lookup_dict
    :return:
    """
    conditions = lookup_dict[index].get("conditions")
    attribute_name = lookup_dict[index].get("name")

    if not attribute_exists(xml_tree, attribute_name):
        if attribute_name in alternative_attribute_dict.keys():
            attribute_name = alternative_attribute_dict.get(attribute_name)
        else:
            return []

    attributes = xml_tree.findall('.//Field[@Name="' + attribute_name + '"]')  # find all attributes
    suitable_attributes = []

    if len(conditions) == 0:  # if no conditions
        suitable_attributes = [attribute.text for attribute in attributes]
    else:
        # parent_map to make sure we have belonging together attributes
        parent_map = dict((c, p) for p in xml_tree.iter() for c in p)

        for attribute in attributes:
            if attribute_name == "FlowAchievementNumSubjects":
                # condition checked in sum_attributes so no check_conditions is necessary
                suitable_attributes.append(attribute.text)
                continue
            if check_conditions(attribute, conditions, parent_map):  # all conditions fulfilled
                suitable_attributes.append(attribute.text)

    return suitable_attributes


def find_suitable_struct(structs, conditions):
    """
    Given multiple structs where we only want one struct according to our conditions. The restrictive
    attributes have to be in the struct.
    :param structs:
    :param conditions:
    :return:
    """
    for struct in structs:
        for attribute_name in conditions:
            if conditions.get(attribute_name) == struct.find('.//Field[@Name="' + attribute_name + '"]').text:
                return struct
    return None


def add_to_knowledge_base(subject, values, lookup_table, index):
    """
    Adds triples to the KB where subject and predicate are the same.
    :param index:
    :param lookup_table:
    :param subject:
    :param values:
    :return:
    """
    if values is None:
        return

    if isinstance(values, list):
        for obj in values:
            if isinstance(obj, str) and obj.isdigit():  # makes string digits to ints
                obj = int(obj)
            knowledge_base.add((subject, get_predicate(lookup_table, index), Literal(obj)))
    else:
        if type(values) == str and values.isdigit():
            values = int(values)
        knowledge_base.add((subject, get_predicate(lookup_table, index), Literal(values)))


def get_predicate(table, index):
    """
    Looks up the predicate.
    :param table: Table where predicate is stored
    :param index: Index in table
    :return: A predicate for subject, predicate, object in RDF
    """
    return table[index].get("predicate")


def fill_entity_clinicalTrial(study_xml_tree):
    """
    Method for saving data of entity study for a particular clinical trial.
    :param study_xml_tree: Particular study data necessary for filling entity
    :return: Id in the KB (URL of clinical trial in clinicaltrials.gov)
    """
    nct_id = study_xml_tree.find('.//Field[@Name="NCTId"]').text
    study_id = URIRef(CTRO + nct_id)
    knowledge_base.add((study_id, RDF.type, CTRO_ClinicalTrial))

    for i in range(0, len(lookup_table_study)):
        if lookup_table_study[i].get("name") == "FlowAchievementNumSubjects":
            objects = sum_attributes(study_xml_tree, i, lookup_table_study)
        else:
            objects = find_attributes(study_xml_tree, i, lookup_table_study)

        add_to_knowledge_base(study_id, objects, lookup_table_study, i)

    return study_id


def fill_entity_publication(study_id, study):
    nct_id = study.find('.//Field[@Name="NCTId"]').text
    publication_id = URIRef(nct_id + "_publication")

    knowledge_base.add((publication_id, RDF.type, CTRO_Publication))
    knowledge_base.add((publication_id, CTRO_describes, study_id))

    for i in range(0, len(lookup_table_publication)):
        predicate = get_predicate(lookup_table_publication, i)
        if predicate == CTRO_hasPublicationYear:
            value = find_attributes(study, i, lookup_table_publication)
            if len(value) >= 1:
                value = value[0]
            else:
                continue
            value = re.search("\d{4}", value).group()  # four digits in succession
            add_to_knowledge_base(publication_id, value, lookup_table_publication, i)
            continue  #

        add_to_knowledge_base(publication_id,
                              find_attributes(study, i, lookup_table_publication),
                              lookup_table_publication,
                              i)


def fill_entity_population(study_id, study_xml_tree):
    nct_id = study_xml_tree.find('.//Field[@Name="NCTId"]').text
    population_id = URIRef(nct_id + "_population")

    knowledge_base.add((population_id, RDF.type, CTRO_Population))
    knowledge_base.add((study_id, CTRO_hasPopulation, population_id))

    for i in range(0, len(lookup_table_population)):
        add_to_knowledge_base(population_id,
                              find_attributes(study_xml_tree, i, lookup_table_population),
                              lookup_table_population,
                              i)


def fill_entity_arm(study_id, study_struct):
    nct_id = study_struct.find('.//Field[@Name="NCTId"]').text
    arm_xml_tree_list = study_struct.findall('.//Struct[@Name="ArmGroup"]')  # find all arms
    arm_id_list = []
    flow_milestone_list = study_struct.findall('.//Struct[@Name="FlowMilestone"]')

    for i in range(0, len(arm_xml_tree_list)):  # for each arm
        arm_id = URIRef(nct_id + "_arm_" + str(i))  # generate key
        arm_id_list.append(arm_id)
        knowledge_base.add((arm_id, RDF.type, CTRO_Arm))
        knowledge_base.add((study_id, CTRO_hasArm, arm_id))  # make relations

        for j in range(0, len(lookup_table_arm)):  # for each attribute
            if lookup_table_arm[j].get("name") == "FlowAchievementNumSubjects":
                pivot_struct = find_suitable_struct(flow_milestone_list, lookup_table_arm[j].get("conditions"))
                if pivot_struct is None:
                    continue
                value = pivot_struct.findall('.//Field[@Name="FlowAchievementNumSubjects"]')
                add_to_knowledge_base(arm_id, [value[i].text], lookup_table_arm, j)
            else:
                predicate = get_predicate(lookup_table_arm, j)
                if predicate == CTRO_hasAvgAge:
                    baseline_index = int(arm_id.split("_")[2])
                    avg_age_bm_struct_list = \
                        [x for x in study_struct.findall('.//Struct[@Name="BaselineMeasure"]')
                         if "age" in x.find('.//Field[@Name="BaselineMeasureTitle"]').text.lower() and
                         x.find('.//Field[@Name="BaselineMeasureUnitOfMeasure"]').text.lower() == "years"]
                    avg_age_bm_struct = avg_age_bm_struct_list[0] if len(avg_age_bm_struct_list) == 1 else None
                    if avg_age_bm_struct is None:
                        continue
                    avg_age = [y.find('.//Field[@Name="BaselineMeasurementValue"]')
                               for y in avg_age_bm_struct.findall('.//Struct[@Name="BaselineMeasurement"]')]
                    avg_age = avg_age[baseline_index].text if len(avg_age) > baseline_index else None
                    add_to_knowledge_base(arm_id, avg_age, lookup_table_arm, j)
                    continue

            add_to_knowledge_base(arm_id,
                                  find_attributes(arm_xml_tree_list[i], j, lookup_table_arm),
                                  lookup_table_arm,
                                  j)

        fill_entity_outcome(arm_id, study_struct)
        fill_entity_outcome_adverse_effect(arm_id, study_struct)

    # return value needed to add medication and intervention accordingly
    return dict(zip(arm_id_list, arm_xml_tree_list))


def fill_entity_intervention(study_xml_tree, arm_id_struct_dict):
    nct_id = study_xml_tree.find('.//Field[@Name="NCTId"]').text
    intervention_list = study_xml_tree.findall('.//Struct[@Name="Intervention"]')

    intervention_id_list = [URIRef(nct_id + "_" + "intervention_" + str(i)) for i in range(0, len(intervention_list))]

    # construct ArmGroupInterventionName of intervention attributes
    intervention_label_struct_list = [(intervention.find('.//Field[@Name="InterventionType"]').text + ": " +
                                       intervention.find('.//Field[@Name="InterventionName"]').text, intervention)
                                      for intervention in intervention_list]

    # put interventions in kb
    for i in range(0, len(intervention_id_list)):
        knowledge_base.add((intervention_id_list[i], RDF.type, CTRO_Intervention))

    intervention_label_id_dict = dict(zip([x for x, _ in intervention_label_struct_list], intervention_id_list))
    intervention_id_struct_dict = dict(zip(intervention_id_list, intervention_list))
    intervention_id_arm_id_mapping = {}  # needed for filling entity intervention

    # associate arms with interventions
    for arm_id, xml_tree in arm_id_struct_dict.items():
        # lookup key
        arm_label_struct_list = [(label.text, xml_tree) for label in
                                 xml_tree.findall('.//Field[@Name="ArmGroupInterventionName"]')]
        for arm_label, _ in arm_label_struct_list:
            # ArmGroupInterventionName == InterventionType + ": " + InterventionName
            if arm_label in intervention_label_id_dict.keys():  # if association exists
                intervention_id_arm_id_mapping[intervention_label_id_dict.get(arm_label)] = arm_id
                knowledge_base.add((arm_id,
                                    CTRO_hasIntervention,
                                    intervention_label_id_dict.get(arm_label)))

    # unite dicts and lists to one list of tuples with id of intervention and structs intervention and arm
    intervention_id_structs_list = [(i_id, i_struct, a_struct)
                                    for i_id, i_struct in intervention_id_struct_dict.items()
                                    for a_id, a_struct in arm_id_struct_dict.items()
                                    if intervention_id_arm_id_mapping.get(i_id) == a_id]

    # fill single interventions
    for i in range(0, len(intervention_id_structs_list)):  # for every intervention
        intervention_id = intervention_id_structs_list[i][0]
        intervention_struct = intervention_id_structs_list[i][1]
        arm_struct = intervention_id_structs_list[i][2]

        # arm description + intervention name + if exists description of intervention
        description = intervention_struct.find('.//Field[@Name="InterventionType"]').text + ". " + intervention_struct. \
            find('.//Field[@Name="InterventionName"]').text

        if attribute_exists(intervention_struct, "InterventionDescription"):  # add description if provided
            description += " " + intervention_struct.find('.//Field[@Name="InterventionDescription"]').text
        else:
            description += " " + arm_struct.find('.//Field[@Name="ArmGroupDescription"]').text

        for j in range(0, len(lookup_table_intervention)):  # add all attributes in intervention
            predicate = get_predicate(lookup_table_intervention, j)
            value = ""

            if predicate == CTRO_hasFrequency:
                # searches all occurrences of weekly and daily.
                value = re.findall("[\w]*[\s](?:daily|weekly)", description)
            elif predicate == CTRO_hasInterval:
                # TODO, not sure what I should search for
                # value = description
                pass
            elif predicate == CTRO_hasDuration:
                # TODO some thinks not included like 12, 24, or 48 weeks
                value = re.findall("\d+\s(?:days|weeks|month)", description)
            elif predicate == CTRO_hasRelativeFreqTime:
                value = re.findall("\w+\s(?:dinner|breakfast)", description)

            if len(value) == 0 or value is None:
                continue

            if isinstance(value, list):
                if len(value) > 1:
                    value.append("Multiple Values found. Description provided: " + description)

                for k in range(0, len(value)):
                    x = value[k]
                    value[k] = x[1:] if x[0] == ' ' else x  # delete space at the beginning

            add_to_knowledge_base(intervention_id, value, lookup_table_intervention, j)

        # fill entity medication
        fill_entity_medication(intervention_id, description)


def fill_entity_medication(intervention_id, description):
    medication_id = URIRef(str(intervention_id).replace("intervention", "medication"))
    knowledge_base.add((medication_id, RDF.type, CTRO_Medication))
    knowledge_base.add((intervention_id, CTRO_hasMedication, medication_id))
    re_pattern_dose_value = "\d+[.]*\d*"  # covers all real numbers

    # TODO add more potential units
    re_pattern_dose_unit = "(?:mg|mL|mcG|micrograms)"
    # TODO enumerations are not working
    re_pattern_dose_properties = " " + re_pattern_dose_value + "(?:|-| )" + re_pattern_dose_unit + "(?:.|,| )"

    for i in range(0, len(lookup_table_medication)):
        predicate = get_predicate(lookup_table_medication, i)
        value = None

        if predicate == CTRO_hasDrug:
            value = "Drug" in description
        elif predicate == CTRO_hasDoseValue:
            value = re.findall(re_pattern_dose_properties, description)
            if value is not None:
                value = [y for x in value for y in re.findall(re_pattern_dose_value, x)]
        elif predicate == CTRO_hasDoseUnit:
            value = re.findall(re_pattern_dose_properties, description)
            if value is not None:
                value = [re.search(re_pattern_dose_unit, x).group() for x in value]
        elif predicate == CTRO_hasDeliveryMethod:
            # TODO are there more methods? -> add
            value = re.findall("(?:orally|mouth|injection)", description)
            value = [method if method != "mouth" else "orally" for method in value]  # replace mouth with orally
        elif predicate == CTRO_hasApplicationCondition:
            # TODO enumerations like Breakfast and Supper
            value = re.findall("((?:after|before)* (?:breakfast|dinner|supper|meal|meals))", description)

        if value is None:
            # TODO what to do if nothing is found. Provide Description or simply add nothing
            value = "Could not extract or not provided. Here is the full description: " + description

        if isinstance(value, list):
            value = list(dict.fromkeys(value))  # eliminate duplicates
            if len(value) > 1:
                value.append("Multiple Values found. Description provided: " + description)

        add_to_knowledge_base(medication_id, value, lookup_table_medication, i)


def fill_entity_outcome(arm_id, study_xml_tree):
    arm_index = int(arm_id[-1:])

    baseline_group_id = study_xml_tree.findall('.//Struct[@Name="BaselineGroup"]')[arm_index].find(
        './/Field[@Name="BaselineGroupId"]').text  # one baseline measure per study
    outcome_id = "OG" + ("00" if arm_index < 10 else "0") + str(arm_index)  # construct outcome-group id

    outcome_measure_list = study_xml_tree.findall('.//Struct[@Name="OutcomeMeasure"]')
    for i in range(0, len(outcome_measure_list)):  # for every outcome
        # for every outcome get the mentioned arms
        pivot_outcome_measure_struct = outcome_measure_list[i]
        outcome_group_id_list = [group.find('.//Field[@Name="OutcomeGroupId"]').text
                                 for group in pivot_outcome_measure_struct.findall('.//Struct[@Name="OutcomeGroup"]')]

        # arm is not mentioned in outcome report -> skip this outcome
        if outcome_id not in outcome_group_id_list:
            continue

        # outcome can have multiple measurements and data stored in analysis and class -> new instance for each measure
        outcome_class_list = pivot_outcome_measure_struct.findall('.//Struct[@Name="OutcomeClass"]')
        outcome_analysis_list = pivot_outcome_measure_struct.findall('.//Struct[@Name="OutcomeAnalysis"]')

        baseline_measure_struct_list = [
            bm_struct for bm_struct in study_xml_tree.findall('.//Struct[@Name="BaselineMeasure"]')
            if bm_struct.find('.//Field[@Name="BaselineMeasureUnitOfMeasure"]').text ==
               pivot_outcome_measure_struct.find('.//Field[@Name="OutcomeMeasureUnitOfMeasure"]').text]
        pivot_baseline_measure_struct = None
        if len(baseline_measure_struct_list) > 0:
            pivot_baseline_measure_struct = baseline_measure_struct_list[0]

        outcome_entity_list = []
        for j in range(0, len(outcome_class_list)):
            outcome_uri = URIRef(arm_id[0:11] + "_outcome_" + str(arm_index) + "_" + str(i)) + "_" + str(j)
            outcome_entity_list.append(outcome_uri)
            knowledge_base.add((outcome_uri, RDF.type, CTRO_Outcome))
            knowledge_base.add((arm_id, CTRO_hasOutcome, outcome_uri))

            pivot_baseline_measurement = None
            pivot_bl_outcome_measurement = None

            # set baseline element where the data for baseline-values is extracted from
            #
            if pivot_baseline_measure_struct is not None:
                pivot_baseline_measurement = \
                    [bm for bm in pivot_baseline_measure_struct.findall('.//Struct[@Name="BaselineMeasurement"]')
                     if bm.find('.//Field[@Name="BaselineMeasurementGroupId"]').text == baseline_group_id]
                if len(pivot_baseline_measurement) > 0:
                    pivot_baseline_measurement = pivot_baseline_measurement[0]
                else:
                    pivot_baseline_measurement = None
            else:
                # if only one endpoint exists and no baseline measures do not fill baseline with first endpoint
                if len(outcome_class_list) > 1:
                    # get the first class and thus first endpoint
                    first_outcome_class = pivot_outcome_measure_struct.find('.//Struct[@Name="OutcomeClass"]')
                    pivot_bl_outcome_measurement = \
                        [measurement for measurement in
                         first_outcome_class.findall('.//Struct[@Name="OutcomeMeasurement"]')
                         if measurement.find('.//Field[@Name="OutcomeMeasurementGroupId"]').text == outcome_id]
                    if len(pivot_bl_outcome_measurement) > 0:
                        pivot_bl_outcome_measurement = pivot_bl_outcome_measurement[0]
                    else:
                        pivot_bl_outcome_measurement = None

            bl_value = None
            res_value = None
            change_value = None

            pivot_outcome_class_struct = outcome_class_list[j] if len(outcome_class_list) > j else \
                outcome_class_list[0]
            # select pivot_outcome_analysis_struct
            if len(outcome_analysis_list) > len(outcome_class_list):
                pivot_outcome_analysis_struct = [x
                                                 for x in outcome_analysis_list
                                                 for y in x.findall('.//Field[@Name="OutcomeAnalysisGroupId"]')
                                                 if y.text == outcome_id]
                pivot_outcome_analysis_struct = pivot_outcome_analysis_struct[0] if len(
                    pivot_outcome_analysis_struct) >= 1 else None
            else:
                pivot_outcome_analysis_struct = (
                    outcome_analysis_list[j] if len(outcome_analysis_list) > j else outcome_analysis_list[0]) \
                    if len(outcome_analysis_list) > 0 else None

            # fill all values
            for k in range(0, len(lookup_table_outcome)):
                predicate = get_predicate(lookup_table_outcome, k)
                value = None

                # switch case for different predicates
                if predicate == CTRO_hasOutcomeType:
                    value = pivot_outcome_measure_struct.find('.//Field[@Name="OutcomeMeasureType"]').text
                elif predicate == CTRO_hasBaselineValue:
                    if pivot_baseline_measurement is not None:
                        value = pivot_baseline_measurement.find('.//Field[@Name="BaselineMeasurementValue"]').text
                    elif pivot_bl_outcome_measurement is not None:
                        value = pivot_bl_outcome_measurement.find('.//Field[@Name="OutcomeMeasurementValue"]').text
                    bl_value = value
                elif predicate == CTRO_hasPValueBL:
                    # rarely provided
                    pass
                elif predicate == CTRO_hasStandardDevBL:
                    # baseline struct with according information exists
                    if pivot_baseline_measurement is not None:
                        if pivot_baseline_measurement.find('.//Field[@Name="BaselineMeasurementSpread"]') is not None:
                            value = pivot_baseline_measurement.find('.//Field[@Name="BaselineMeasurementSpread"]').text
                    elif pivot_bl_outcome_measurement is not None:
                        if pivot_bl_outcome_measurement.find('.//Field[@Name="OutcomeMeasurementSpread"]') is not None:
                            value = pivot_bl_outcome_measurement.find('.//Field[@Name="OutcomeMeasurementSpread"]').text
                elif predicate == CTRO_hasStandardErrorBL:
                    # rarely provided
                    pass
                elif predicate == CTRO_hasConfidenceIntervalBL:
                    # rarely provided
                    pass
                elif predicate == CTRO_hasResultMeasuredValue:
                    lookup_table_outcome[k]["conditions"]["OutcomeMeasurementGroupId"] = outcome_id
                    value = find_attributes(pivot_outcome_class_struct, k, lookup_table_outcome)
                    if len(value) == 1:
                        res_value = value[0]
                elif predicate == CTRO_hasPValueResValue:
                    if pivot_outcome_analysis_struct is None:
                        continue
                    value = find_attributes(pivot_outcome_analysis_struct, k, lookup_table_outcome)
                elif predicate == CTRO_hasStandardDevResValue:
                    if pivot_outcome_analysis_struct is None:
                        continue
                    if attribute_exists(pivot_outcome_analysis_struct, "OutcomeAnalysisDispersionValue") and \
                            attribute_exists(pivot_outcome_analysis_struct, "OutcomeAnalysisDispersionType"):
                        if "standard deviation" in pivot_outcome_analysis_struct.find(
                                './/Field[@Name="OutcomeAnalysisDispersionType"]').text.lower():
                            value = pivot_outcome_analysis_struct.find(
                                './/Field[@Name="OutcomeAnalysisDispersionValue"]').text
                elif predicate == CTRO_hasStandardErrorResValue:
                    if pivot_outcome_analysis_struct is None:
                        continue
                    if attribute_exists(pivot_outcome_analysis_struct, "OutcomeAnalysisDispersionValue") and \
                            attribute_exists(pivot_outcome_analysis_struct, "OutcomeAnalysisDispersionType"):
                        if "standard error" in pivot_outcome_analysis_struct.find(
                                './/Field[@Name="OutcomeAnalysisDispersionType"]').text.lower():
                            value = pivot_outcome_analysis_struct.find(
                                './/Field[@Name="OutcomeAnalysisDispersionValue"]').text
                elif predicate == CTRO_hasConfidenceIntervalResValue:
                    if pivot_outcome_analysis_struct is None:
                        continue
                    value = find_attributes(pivot_outcome_analysis_struct, k, lookup_table_outcome)
                elif predicate == CTRO_hasChangeValue:
                    if bl_value is not None and res_value is not None:
                        try:
                            value = float(res_value) - float(bl_value)
                            if value == 0:
                                value = value
                            change_value = value
                        except ValueError:
                            # value could not be casted, mostly string "NA"
                            value = "NA"
                elif predicate == CTRO_hasPValueChangeValue:
                    # needs to be extracted, rarely provided
                    pass
                elif predicate == CTRO_hasStandardDevChangeVal:
                    # needs to be extracted, rarely provided
                    pass
                elif predicate == CTRO_hasStandardErrorChangeVal:
                    # needs to be extracted, rarely provided
                    pass
                elif predicate == CTRO_hasConfidenceIntervalChangeVal:
                    # needs to be extracted, rarely provided
                    pass
                elif predicate == CTRO_hasRelativeChangeValue:
                    # needs to be extracted, rarely provided
                    pass
                elif predicate == CTRO_hasObservedResult:
                    value = find_attributes(pivot_outcome_measure_struct, k, lookup_table_outcome)
                elif predicate == CTRO_hasTimePoint:
                    pass
                elif predicate == CTRO_hasSubGroupDescription:
                    # TODO, OutcomeGroupTitle + OutcomeGroupDescription in OutcomeGroupList where OutcomeGroupId == outcome_id
                    # Check endpoint-description then
                    struct = find_suitable_struct(
                        pivot_outcome_measure_struct.findall('.//Struct[@Name="OutcomeGroup"]'),
                        {"OutcomeGroupId": outcome_id})
                    value = struct.find('.//Field[@Name="OutcomeGroupTitle"]').text + ". " + \
                            struct.find('.//Field[@Name="OutcomeGroupDescription"]').text

                elif predicate == CTRO_hasChangeDirection:
                    if change_value is not None:
                        if change_value < 0:
                            value = '-'
                        elif change_value > 0:
                            value = '+'
                        else:  # should rarely occur
                            value = '='
                else:
                    # attribute for adverse effects
                    continue

                if value is None:
                    continue

                add_to_knowledge_base(outcome_uri, value, lookup_table_outcome, k)

            # then connect endpoint and and outcome
            fill_entity_endpoint(outcome_uri, study_xml_tree, pivot_outcome_measure_struct, pivot_outcome_analysis_struct)


def fill_entity_outcome_adverse_effect(arm_id, study_xml_tree):
    arm_index = int(arm_id[-1:])
    event_group_id = "EG" + ("00" if arm_index < 10 else "0") + str(arm_index)
    event_list = study_xml_tree.findall('.//Struct[@Name="SeriousEvent"]')  # add all serious events
    # TODO add other_events if wanted [event_list.append(x) for x in study_xml_tree.findall('.//Struct[@Name="OtherEvent"]')]  # add all other events

    for i in range(0, len(event_list)):
        outcome_uri = URIRef(arm_id[0:11] + "_outcome_ae_" + str(arm_index) + "_" + str(i))
        knowledge_base.add((outcome_uri, RDF.type, CTRO_Outcome))
        knowledge_base.add((arm_id, CTRO_hasAdverseEffect, outcome_uri))
        pivot_event = event_list[i]

        is_series_event = attribute_exists(pivot_event, "SeriousEventTerm")
        if is_series_event:  # series event
            field_name = "SeriousEventStatsGroupId"
            struct_name = "SeriousEventStats"
        else:  # other event
            field_name = "OtherEventStatsGroupId"
            struct_name = "OtherEventStats"

        pivot_event_stats = [stats_struct
                             for stats_struct in pivot_event.findall('.//Struct[@Name="' + struct_name + '"]')
                             if stats_struct.find('.//Field[@Name="' + field_name + '"]').text == event_group_id]

        if len(pivot_event_stats) == 0:
            continue  # arm is not listed in adverse effect
        else:
            pivot_event_stats = pivot_event_stats[0]

        for k in range(0, len(lookup_table_outcome)):
            predicate = get_predicate(lookup_table_outcome, k)
            value = None

            if predicate == CTRO_hasNumberAffected:
                field_name = "SeriousEventStatsNumAffected" if is_series_event else "OtherEventStatsNumAffected"
                value = pivot_event_stats.find('.//Field[@Name="' + field_name + '"]').text
            elif predicate == CTRO_hasPercentageAffected:
                # Not provided
                pass
            elif predicate == CTRO_hasPValueNumAffected:
                # Not provided
                pass
            elif predicate == CTRO_hasStandardDevNumAffected:
                # Not provided
                pass
            elif predicate == CTRO_hasStandardErrorNumAffected:
                # Not provided
                pass
            elif predicate == CTRO_hasConfidenceIntervalNumAffected:
                # Not provided
                pass
            else:
                # attribute for general outcome
                continue

            if value is None:
                continue

            add_to_knowledge_base(outcome_uri, value, lookup_table_outcome, k)
            fill_entity_endpoint_adverse_effect(outcome_uri, study_xml_tree, pivot_event, is_series_event)


def fill_entity_endpoint_adverse_effect(outcome_uri, study_xml_tree, pivot_event, is_series):
    endpoint_uri = URIRef(outcome_uri[0:11] + "_endpoint_" + outcome_uri[20:])

    knowledge_base.add((endpoint_uri, RDF.type, CTRO_Endpoint))
    knowledge_base.add((outcome_uri, CTRO_hasEndpoint, endpoint_uri))

    for i in range(0, len(lookup_table_endpoint)):
        predicate = get_predicate(lookup_table_endpoint, i)
        value = None

        if predicate == CTRO_hasEndpointDescription:
            if is_series:  # series event
                field_name = "SeriousEventTerm"
            else:  # other event
                field_name = "OtherEventTerm"
            value = pivot_event.find('.//Field[@Name="' + field_name + '"]').text
        elif predicate == CTRO_hasAggregationMethod:
            pass
        elif predicate == CTRO_hasBaselineUnit:
            pass
        elif predicate == CTRO_hasMeasurementDevice:
            pass
        else:
            print("something went wrong")
            continue

        if value is None:
            continue

        add_to_knowledge_base(endpoint_uri, value, lookup_table_endpoint, i)


def fill_entity_endpoint(outcome_uri, study_xml_tree, pivot_outcome_measure_struct, pivot_outcome_analysis_struct):
    endpoint_uri = URIRef(outcome_uri[0:11] + "_endpoint_" + outcome_uri[20:])

    knowledge_base.add((endpoint_uri, RDF.type, CTRO_Endpoint))
    knowledge_base.add((outcome_uri, CTRO_hasEndpoint, endpoint_uri))

    for i in range(0, len(lookup_table_endpoint)):
        predicate = get_predicate(lookup_table_endpoint, i)
        value = None

        if predicate == CTRO_hasEndpointDescription:
            value = pivot_outcome_measure_struct.find('.//Field[@Name="OutcomeMeasureDescription"]').text \
                if attribute_exists(pivot_outcome_measure_struct, "OutcomeMeasureDescription") else "" + \
                                                                                                    pivot_outcome_measure_struct.find(
                                                                                                        './/Field[@Name="OutcomeMeasurePopulationDescription"]').text \
                if attribute_exists(pivot_outcome_measure_struct, "OutcomeMeasurePopulationDescription") else ""
        elif predicate == CTRO_hasAggregationMethod:
            if pivot_outcome_analysis_struct is not None:
                value = find_attributes(pivot_outcome_analysis_struct, i, lookup_table_endpoint)
        elif predicate == CTRO_hasBaselineUnit:
            value = find_attributes(pivot_outcome_measure_struct, i, lookup_table_endpoint)
        elif predicate == CTRO_hasMeasurementDevice:
            fda_device = study_xml_tree.find('.//Field[@Name="IsFDARegulatedDevice"]')
            if fda_device is not None:
                fda_device = fda_device.text
            unapproved_device = study_xml_tree.find('.//Field[@Name="IsUnapprovedDevice"]')
            if unapproved_device is not None:
                unapproved_device = unapproved_device.text
            value = fda_device == "True" or unapproved_device == "True"
        else:
            print("something went wrong")

        if value is None:
            continue

        add_to_knowledge_base(endpoint_uri, value, lookup_table_endpoint, i)


def fill_entity_diffBetweenGroups():
    """
    This method would fill the data of entity DiffBetweenGroups but the data is not contained in ct.gov.
    """


def fill_entity_evidenceQuality(study_id, study_xml_tree):
    quality_id = URIRef(study_id + "_evidence")

    knowledge_base.add((quality_id, RDF.type, CTRO_EvidenceQuality))
    knowledge_base.add((study_id, CTRO_hasEvidenceQualityIndicator, quality_id))

    # CTRO_isPharmacySponsored
    sponsor_type = study_xml_tree.find('.//Field[@Name="LeadSponsorClass"]').text
    knowledge_base.add((quality_id, CTRO_hasSponsorType, Literal(sponsor_type)))


def setup_knowledge_base():
    tree = et.parse('../resources/data.xml')
    study_list = tree.findall("*/FullStudy")  # list of all studies in document

    for study_xml_tree in study_list:
        study_id = fill_entity_clinicalTrial(study_xml_tree)
        fill_entity_publication(study_id, study_xml_tree)
        fill_entity_population(study_id, study_xml_tree)
        arm_dict = fill_entity_arm(study_id, study_xml_tree)
        fill_entity_intervention(study_xml_tree, arm_dict)
        fill_entity_evidenceQuality(study_id, study_xml_tree)

    ttl_file = open("../resources/knowledge_base.ttl", "w", "utf-8")
    ttl_file.write(knowledge_base.serialize(format="turtle").decode("utf-8"))


if __name__ == "__main__":
    rdfextras.registerplugins()

    start_url = time.time()
    # ct_api.retrieve_clinical_trials(True)
    end_url = time.time()

    start_kb = time.time()
    setup_knowledge_base()
    end_kb = time.time()

    print("Time for getting the data: ", end_url - start_url)
    print("Time for processing the data: ", end_kb - start_kb)
